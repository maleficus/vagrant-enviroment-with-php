<?php
include "helpers.php";
foreach (glob(__DIR__."/Classes/*.php") as $filename)
{
    include $filename;
}
Config::setConfigPath(realpath("/vagrant/temp/config.json"));
Nginx::startProcess();
Composer::startProcess();
Bash::startProcess();
PhpConfig::startProcess();
Mysql::startProcess();
