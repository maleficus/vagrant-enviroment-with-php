<?php
function pre($dump)
{
    echo "<pre>";
    var_dump($dump);
    echo "</pre>";
}

function fillOnUndefined($variable,$index = null,$fillWith = null)
{
  return isset($variable[$index])?$variable[$index]:$fillWith;
}

function array_keys_recursive($myArray, $MAXDEPTH = INF, $depth = 0, $arrayKeys = array())
{
  if($depth < $MAXDEPTH){
       $depth++;
       $keys = array_keys($myArray);
       foreach($keys as $key){
           if(is_array($myArray[$key])){
               $arrayKeys[$key] = array_keys_recursive($myArray[$key], $MAXDEPTH, $depth);
           }
       }
   }

   return $arrayKeys;
}

function dottedArray($index,$targetArray)
{
  $indexes = explode(".",$index);
  $result = $targetArray;
  foreach ($indexes as $perIndex)
  {
    $result = fillOnUndefined($result,$perIndex);
  }
  return $result;
}

function bash_command_exists($command)
{
  $script =
  '
  {
  if type "'.$command.'"; then
  result="1"
  else
  result="0"
  fi
  }> /dev/null
  echo "${result}"
  ';
  return shell_exec($script) == 1?true:false;
}

function file_force_contents($dir, $contents){
    $parts = explode('/', $dir);
    $file = array_pop($parts);
    $dir = '';
    foreach($parts as $part)
        if(!is_dir($dir .= "/$part")) mkdir($dir);
    file_put_contents("$dir/$file", $contents);
}
