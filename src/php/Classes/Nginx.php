<?php
class Nginx
{
  public static function startProcess()
  {
    self::setNginxConfig();
    self::setNginxSites();
    self::setSitesHosts();
    self::cleanUp();
  }

  private static function setNginxConfig()
  {
    exec("sudo cp ".Config::getTemplatesPath()."/nginx.conf /etc/nginx/nginx.conf");
  }

  private static function setNginxSites()
  {
    $nginxSites = Config::getConfig("nginx.sites");
    exec("sudo rm -rf /etc/nginx/sites-available/*");
    exec("sudo rm -rf /etc/nginx/sites-enabled/*");
    $siteTemplate = file_get_contents(Config::getTemplatesPath()."/site-template");
    //Put Sites And Make Links//
    foreach ($nginxSites as $perSite)
    {
      foreach ($perSite["locations"] as $perLocation)
      {
        $fullFolderPath = rtrim(rtrim($perSite["root"],"/")."/".rtrim($perLocation["site_folder"],"/"),"/");
        $modified = str_replace("[site_folder]",$fullFolderPath,$siteTemplate);
        $modified = str_replace("[site_domain]",rtrim($perLocation["site_domain"],"/"),$modified);
        $siteFilePath = "/etc/nginx/sites-available/".rtrim($perLocation["site_domain"],"/");
        $linkPath = "/etc/nginx/sites-enabled/".rtrim($perLocation["site_domain"],"/");
        file_put_contents($siteFilePath,$modified);
        exec("sudo ln -s ".$siteFilePath." ".$linkPath);
      }
    }
    //End //
  }
  private static function setSitesHosts()
  {
    //Put Sites On hosts File
    $nginxSites = Config::getConfig("nginx.sites");
    $hostTemplate = file_get_contents(Config::getTemplatesPath()."/hosts");
    $modified = "";
    foreach ($nginxSites as $perSite)
    {
      foreach ($perSite["locations"] as $perLocation)
      {
        $modified .= "127.0.0.1       ".$perLocation["site_domain"]."\n";
      }
    }
    file_put_contents("/etc/hosts",$modified.$hostTemplate);
    //End//
  }
  private static function cleanUp()
  {
    exec("sudo service nginx restart");
  }
}
