<?php
class PhpConfig
{
  public static function startProcess()
  {
    self::setBashFile();
    self::cleanUp();
  }

  public static function setBashFile()
  {
    echo exec("sudo cp ".Config::getTemplatesPath()."/php.ini /etc/php/5.6/fpm/php.ini");
  }


  private static function cleanUp()
  {
    exec("sudo service php5.6-fpm restart");
  }

}
