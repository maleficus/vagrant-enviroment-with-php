<?php
class Config
{
  private static $configPath;
  private static  $config;


  public static function setConfigPath($path)
  {
    self::$configPath = $path;
    self::$config = json_decode(file_get_contents($path),true);
  }

  public static function getAllConfig()
  {
    return self::$config;
  }

  public static function getConfig($index)
  {

    if(method_exists(get_class(),$index))
    {
      return self::$index();
    }

    return dottedArray($index,self::$config);
  }

  public static function getHostsFilePath()
  {
    return "/etc/hosts";
  }

  public static function getTemplatesPath()
  {
    return rtrim("/vagrant/src/templates","/");
  }

}
