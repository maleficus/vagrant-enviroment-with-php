<?php
class Mysql
{
  public static function startProcess()
  {
    self::setBashFile();
    self::cleanUp();
  }

  public static function setBashFile()
  {
    echo exec("sudo cp ".Config::getTemplatesPath()."/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf");
  }


  private static function cleanUp()
  {
    exec("sudo service mysql restart");
  }

}
